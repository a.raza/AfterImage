﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;
using System.Linq;

public class BuildMesh : MonoBehaviour
{
    public Vector3[] vertices;
    public int[] triangles;
    public Vector2[] uvs;

    //public bool makeCopy;
    public float fDelay, sDelay;
    public int v, t, u;
    //public List<Vector3> vertices = new List<Vector3>();
    //public List<int> triangles = new List<int>();
    //public List<Vector2> uvs = new List<Vector2>();
    private Mesh mesh;
    public SkinnedMeshRenderer sMesh;
    //public bool v = false, t = false, u = false;

    private void Start()
    {
        StartCoroutine(MeshGenerator());
    }

    IEnumerator MeshGenerator() 
    {
        yield return new WaitForSeconds(fDelay);

        MeshFilter mf = GetComponent<MeshFilter>();
        mesh = mf.mesh;
        //mesh.Clear();
        //sMesh.BakeMesh(mesh);
        //if (makeCopy == false)
        //{
        //triangles = mesh.triangles;
        //vertices = mesh.vertices;
        //uvs = mesh.uv;

        var vv = vertices;
        var tt = triangles;
        var uu = uvs;

        vertices = new Vector3[vv.Length];
        for(int i = 0; i < vertices.Length; i++) 
        {
            try 
            {
                vertices[i] = vv[i + v];
            }
            catch 
            {
                break; 
            }
        }

        triangles = new int[tt.Length];
        for (int i = 0; i < triangles.Length; i++)
        {
            try
            {
                triangles[i] = tt[i + t];
            }
            catch
            {
                break;
            }
        }

        uvs = new Vector2[uu.Length];
        for (int i = 0; i < uvs.Length; i++)
        {
            try
            {
                uvs[i] = uu[i + u];
            }
            catch
            {
                break;
            }
        }

        Debug.Log("Before");
        Debug.Log("triangles: " + mesh.triangles.Length);
        Debug.Log("vertices: " + mesh.vertices.Length);
        Debug.Log("uvs: " + mesh.uv.Length);

        //for (int i = 0; i < mesh.triangles.Length; i++)
        //{
        //    i += (int)space;
        //    yield return new WaitForSeconds(sDelay);
        //    try
        //    {
        //        triangles.Add(mesh.triangles[i]);
        //    }
        //    catch
        //    {
        //        break;
        //    }
        //}

        //t = true;

            //for (int i = 0; i < mesh.vertices.Length; i++)
            //{
            //    i += (int)space;
            //    yield return new WaitForSeconds(sDelay);
            //    try
            //    {
            //        vertices.Add(mesh.vertices[i]);
            //    }
            //    catch
            //    {
            //        break;
            //    }
            //}

            //v = true;

            //for (int i = 0; i < mesh.uv.Length; i++)
            //{
            //    i += (int)space;
            //    yield return new WaitForSeconds(sDelay);
            //    try
            //    {
            //        uvs.Add(mesh.uv[i]);
            //    }
            //    catch
            //    {
            //        break;
            //    }
            //}

            //u = true;
        //}
        //else 
        //{
        //    t = true;
        //    v = true; 
        //    u = true;
        //}

        //if(t == true && v == true && u == true) 
        //{
            Debug.Log("After");
            Debug.Log("triangles: " + triangles.Length);
            Debug.Log("vertices: " + vertices.Length);
            Debug.Log("uvs: " + uvs.Length);

            mesh.Clear();

            mesh.vertices = vertices;
            mesh.triangles = triangles;
            mesh.uv = uvs; 
            MeshUtility.Optimize(mesh);
            mesh.RecalculateNormals();
        //}

        StartCoroutine(MeshGenerator());
    }
}
