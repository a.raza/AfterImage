﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.IO;

public class EZIO  
{
    private string 
    m_path = string.Empty, 
    m_fileName = string.Empty;

    public byte[][] m_listOfBytes;
    public string[] m_listOfString;

    /// <summary>
    /// At Start; Set a Proper Path.
    /// </summary>
    /// <param name="a_filename">A filename, Example "myTextFile.txt".</param>
    public EZIO(string a_filename)
    {
        m_fileName = a_filename;
        m_path = Application.streamingAssetsPath +"/"+ a_filename;
        //Debug.Log("Path: "+ m_path);
    }

    /// <summary>
    /// Creates a new text file.
    /// </summary>
    /// <param name="a_fileName">A file name.</param>
    /// <param name="a_dataToStore">A data to store in.</param>
    public void CreateFile(string a_fileName, string a_dataToStore)
    {
        string a_path = Application.streamingAssetsPath +"/" + a_fileName;

        if (!File.Exists(a_path))
        {
            File.WriteAllText(a_path, a_dataToStore);
        }
        else
        {
            Debug.Log("You Are Re-creating a file : Method Create File");
        }
    }

    /// <summary>
    /// Creates a new text file.
    /// </summary>
    /// <param name="a_fileName">A file name.</param>
    /// <param name="a_dataToStore">A data to store in.</param>
    public void CreateFile(string a_dataToStore)
    {
        CreateFile(m_fileName, a_dataToStore);
    }


    /// <summary>
    /// Reads data and returns in string.
    /// </summary>
    /// <returns>The data store in string.</returns>
    /// <param name="a_fileName">A file name.</param>
    public string ReadData_StoreInString(string a_fileName)
    {
        string a_path = Application.streamingAssetsPath +"/" + a_fileName;

        if (File.Exists(a_path))
        {
            return File.ReadAllText(a_path);
        }
        else
        {
            Debug.Log("Null : Method Read Data _ To String");

            return null;
        }
    }

    /// <summary>
    /// Reads data and returns in string.
    /// </summary>
    /// <returns>The data store in string.</returns>
    public string ReadData_StoreInString()
    {
        return ReadData_StoreInString(m_fileName);
    }

    /// <summary>
    /// Reads data and stores each line in the list of string.
    /// </summary>
    /// <returns>The data store each line in list of string.</returns>
    /// <param name="a_fileName">A file name.</param>
    /// <param name="a_arrayOfEachLineString">An array in which the data will get stored, line by line in a form of string.</param>
    /// <param name="a_seconds"> how much seconds it should take per 5000 line of text, recommended: 0.015f.</param>
    public IEnumerator ReadData_StoreEachLine_InListOfString(string a_fileName, string[] a_arrayOfEachLineString, float a_seconds)
    {
        string a_path = Application.streamingAssetsPath +"/" + a_fileName;

        if (File.Exists(a_path))
        {
            var a_line = string.Empty;
            var a_file = new StreamReader(a_path);
            a_arrayOfEachLineString = new string[Get_LinesInTextFile(a_fileName)];

            while ((a_line = a_file.ReadLine()) != null)
            {
                for (int i = 0; i < 5000; i++)
                {
                    if ((a_line = a_file.ReadLine()) != null)
                        a_arrayOfEachLineString[i] = a_line;
                    else
                        break;
                }

                yield return new WaitForSeconds(a_seconds);
            }
        }
        else
        {
            Debug.Log("Null : Method Read Data _ Store Each Line _ In List Of String");

            yield return null;
        }
    }

    /// <summary>
    /// Reads data and stores each line in the list of bytes.
    /// </summary>
    /// <returns>The data store each line in list of bytes.</returns>
    /// <param name="a_fileName">A file name.</param>
    /// <param name="a_seconds"> how much seconds it should take per 5000 line of text, recommended: 0.015f.</param>
    public IEnumerator ReadData_StoreEachLine_InListOfBytes(string a_fileName, float a_seconds)
    {
        string a_path = Application.streamingAssetsPath +"/" + a_fileName;

        if (File.Exists(a_path))
        {
            var a_line = string.Empty;
            var a_file = new StreamReader(a_path);
            m_listOfBytes = new byte[Get_LinesInTextFile(a_fileName)][];

            while ((a_line = a_file.ReadLine()) != null)
            {
                for (int i = 0; i < 5000; i++)
                {
                    if ((a_line = a_file.ReadLine()) != null)
                        m_listOfBytes[i] = Encoding.ASCII.GetBytes(a_line);
                    else
                        break;
                }

                yield return new WaitForSeconds(a_seconds);
            }

            a_file.Close();
        }
        else
        {
            Debug.Log("Null : Method Read Data _ Store Each Line _ In List Of String");

            yield return null;
        }
    }

    /// <summary>
    /// Converts data from bytes to string.
    /// </summary>
    /// <returns>The decoder bytes to string.</returns>
    /// <param name="a_bytes">A bytes.</param>
    public string DataDecoder_BytesToString(byte[] a_bytes)
    {
        //var a_data = Encoding.ASCII.GetString(a_bytes);

        //var a_data = Encoding.UTF8.GetString(a_bytes);

        //return a_data;

        string response = string.Empty;

        foreach (byte b in a_bytes)
            response += (char)b;

        return response;
    }

    /// <summary>
    /// Converts the list of bytes to list of string.
    /// </summary>
    /// <returns>The list of bytes to list of string.</returns>
    /// <param name="a_fileName">A file name.</param>
    /// <param name="a_countPerSecond">A count per second.</param>
    public IEnumerator Convert_ListOfBytes_To_ListOfString(string a_fileName,float a_countPerSecond)
    { 
        m_listOfString = new string[Get_LinesInTextFile(a_fileName)];
        
        for (int i = 0; i < Get_LinesInTextFile(a_fileName);)
        {
            for (int j = 0; j < 5000; j++)
            {
                if(i < Get_LinesInTextFile(a_fileName))
                {
                    for (int k = 0; k < m_listOfBytes[i].Length;k++)
                    {
                        var word = DataDecoder_BytesToString(m_listOfBytes[i]);

                        m_listOfString[i] = word;

                        i++;
                    }
                }
                else
                {
                    break;
                }
            }

            yield return new WaitForSeconds(a_countPerSecond);  
        }
    }


    /// <summary>
    /// Writes New data on existing file.
    /// </summary>
    /// <param name="a_fileName">A file name.</param>
    /// <param name="a_dataToStore">A string require in which whole will get store.</param>
    public void WriteDataInExistingFile(string a_fileName, string a_dataToStore)
    {
        string a_path = Application.streamingAssetsPath +"/" + a_fileName;

        if (File.Exists(a_path))
        {
            File.WriteAllText(a_path, a_dataToStore);
        }
        else
        {
            Debug.Log("You Need To Create A New Text File First");
        }
    }

    /// <summary>
    /// Writes New data on existing file.
    /// </summary>
    /// <param name="a_dataToStore">A string require in which whole will get store.</param>
    public void WriteDataInExistingFile(string a_dataToStore)
    {
        WriteDataInExistingFile(m_fileName, a_dataToStore);
    }

    /// <summary>
    /// Clears data of the existing file
    /// </summary>
    /// <param name="a_fileName">A file name.</param>
    public void ClearDate(string a_fileName)
    {
        string a_path = Application.streamingAssetsPath +"/" + a_fileName;

        if (File.Exists(a_path))
        {
            string savedata = string.Empty;
            File.WriteAllText(a_path, "");
        }
        else
        {
            Debug.Log(" File Not Found \n Cannot call ClearDate Method");
        }
    }

    /// <summary>
    /// Clears data of the existing file
    /// </summary>
    public void ClearDate()
    {
        ClearDate(m_fileName);
    }

    /// <summary>
    /// Check's if certain file is available
    /// </summary>
    /// <returns><c>true</c>, if file available was ised, <c>false</c> otherwise.</returns>
    /// <param name="a_fileName">A file name.</param>
    public bool Is_FileAvailable(string a_fileName)
    {
        string a_path = Application.streamingAssetsPath +"/" + a_fileName;
        
        return File.Exists(a_path);
    }

    /// <summary>
    /// Check's if certain file is available
    /// </summary>
    /// <returns><c>true</c>, if file available was ised, <c>false</c> otherwise.</returns>
    public bool Is_FileAvailable()
    {
        return File.Exists(m_path);
    }

    /// <summary>
    /// Gets total amount of chars available in text file.
    /// </summary>
    /// <returns>The chars in text file.</returns>
    /// <param name="a_fileName">A file name.</param>
    public int Get_CharsInTextFile(string a_fileName)
    {
        string a_path = Application.streamingAssetsPath +"/" + a_fileName;

        int a_amountOfChar = File.ReadAllText(m_path).ToCharArray().Length;

        return a_amountOfChar;
    }

    /// <summary>
    /// Gets total amount of chars available in text file.
    /// </summary>
    /// <returns>The chars in text file.</returns>
    public int Get_CharsInTextFile()
    {
        return Get_CharsInTextFile(m_fileName);
    }

    /// <summary>
    /// Gets the amount of lines available in text file.
    /// </summary>
    /// <returns>The lines in text file.</returns>
    public int Get_LinesInTextFile(string a_fileName)
    {
        string a_path = Application.streamingAssetsPath +"/" + a_fileName;

        int a_lines = File.ReadAllLines(m_path).Length;

        return a_lines;
    }

    /// <summary>
    /// Gets the amount of lines available in text file.
    /// </summary>
    /// <returns>The lines in text file.</returns>
    public int Get_LinesInTextFile()
    {
        return Get_LinesInTextFile(m_fileName);
    }


}
