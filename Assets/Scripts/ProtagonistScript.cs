﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ProtagonistScript : MonoBehaviour
{
    #region Public Variables

    public static ProtagonistScript m_instance = null;
    [Range(10, 25)]
    public float m_speed = 10f;
    public Dimension m_dimension;

    #endregion

    #region Private Variables

    internal Controller m_controller;
    private Animator m_animator;
    #endregion

    public GameObject m_skin;

    #region Unity callbacks

    void Awake()
    {
        if (m_instance == null)
            m_instance = this;

        if (GetComponent<Animator>() != null)
            m_animator = GetComponent<Animator>();

        m_controller = new Controller(this.transform, m_speed, m_dimension);
    }

    void Update()
    {
        m_controller.OnUpdate();

        if (m_controller.Is_controllerActive())
        {
            m_controller.Locomote();    //Rotation
            m_controller.Move();        //Movement

            SetAnimation(AnimationClips.WALK);

            if(m_skin != null)
                m_skin.transform.localEulerAngles = new Vector3(0, -transform.eulerAngles.y, 0); //Skin Rotation
        }
        else
        {
            SetAnimation(AnimationClips.IDEL);
        }

        OnPressSlide();
    }

    #endregion

    #region Helping Functions

    private void OnPressSlide()
    {
        if (Input.GetKeyDown(KeyCode.Space) && !IsCurrentAnimation(AnimationClips.SLIDE))
            SetAnimation(AnimationClips.SLIDE);
    }

    private void SetAnimation(AnimationClips a_animationClip)
    {
        if (m_animator != null)
            m_animator.SetInteger("Anim", (int)a_animationClip);
    }

    private bool IsCurrentAnimation(AnimationClips a_animationClip)
    {
        return m_animator.GetCurrentAnimatorStateInfo(0).IsTag(a_animationClip.ToString()).Equals(true);
    }

    #endregion
}

public enum AnimationClips
{
    IDEL = 0,
    WALK = 1,
    SLIDE = 2,
};