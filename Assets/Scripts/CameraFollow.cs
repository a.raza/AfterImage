﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFollow : MonoBehaviour 
{
    public float m_camSpeed= 120.0f;
	public GameObject m_camFollower;
    public float m_clampAngle = 80.0f;
    public float m_inputSensitivity = 150.0f;
    public GameObject m_cam;
	
    private float
    m_rotY = 0.0f,
    m_rotX = 0.0f,
    m_finalInputZ,
    m_finalInputX,
    m_mouseY,
    m_mouseX;

    // Use this for initialization
    void Start () 
    {
		Vector3 rot = transform.localRotation.eulerAngles;
		m_rotY = rot.y;
		m_rotX = rot.x;

		Cursor.lockState = CursorLockMode.Locked;
		Cursor.visible = false;
	}

	void LateUpdate () 
    {
        m_mouseX = Input.GetAxis("Mouse X");
        m_mouseY = Input.GetAxis("Mouse Y");

        m_finalInputX = m_mouseX;
        m_finalInputZ = m_mouseY;

        m_rotY += m_finalInputX * m_inputSensitivity * Time.deltaTime;
        m_rotX += m_finalInputZ * m_inputSensitivity * Time.deltaTime;

        m_rotX = Mathf.Clamp(m_rotX, -m_clampAngle, m_clampAngle);

        Quaternion localRotation = Quaternion.Euler(m_rotX, m_rotY, 0.0f);
        transform.rotation = localRotation;
        
		CameraUpdater ();
	}

	void CameraUpdater() 
    {
		// set the target object to follow
        Transform target = m_camFollower.transform;

		//move towards the game object that is the target
		float step = m_camSpeed * Time.deltaTime;
		transform.position = Vector3.MoveTowards (transform.position, target.position, step);
	}
}
