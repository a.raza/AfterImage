﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataManager : MonoBehaviour 
{
    public EZIO m_ezio;
    public byte[][] m_listOfBytes;
    public string[] m_words;

	// Use this for initialization
	void Start () 
    {
        m_ezio = new EZIO("words.txt");

        StartCoroutine(m_ezio.ReadData_StoreEachLine_InListOfBytes("words.txt", 0.015f));

        StartCoroutine(m_ezio.Convert_ListOfBytes_To_ListOfString("words.txt", 0.015f));
	}
	
}
