﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DistanceTest : MonoBehaviour
{
    public ParticleManagerScript pm;

    public Transform[] cuberds;

    public float spaceBetweenParticles;

    private List<Vector3> m_positions = new List<Vector3>();

    void Start()
    {
        UpdateParticlesPositions();
    }

    public void UpdateParticlesPositions()
    {
        m_positions.Add(cuberds[0].position);

        m_positions.Add(cuberds[1].position);

        float distance = (m_positions[0] - m_positions[1]).magnitude;

        for (float space = 0; space < (distance);)
        {
            var a_position = (Lerp(m_positions[0], m_positions[1], space / distance));

            m_positions.Add(a_position);

            space += spaceBetweenParticles;
        }

        pm.m_positions = m_positions.ToArray();

        pm.gameObject.SetActive(true);

        Debug.Log("Particles Count: " + m_positions.Count);
    }

    Vector3 Lerp(Vector3 start, Vector3 end, float percent)
    {
        return (start + percent * (end - start));
    }
}