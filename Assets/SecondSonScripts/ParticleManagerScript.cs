﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ParticleManagerScript: MonoBehaviour
{
    private ParticleSystem m_particleSystem;

    private ParticleSystem.Particle[] m_particleList;
    internal float afterImageDelay;
    internal float particleSpeed;

    internal Vector3[] m_positions;

    internal ParticleAction particleAction;
    private int m_max;

    internal void OnEnable()
    {
        //getting particle system
        m_particleSystem = GetComponent<ParticleSystem>();

        StartCoroutine(ParticlesPositionController(afterImageDelay));
    }

    internal void ReuseParticles()
    {   
        if (m_positions.Length >= m_max)
            m_max = m_positions.Length;
        
        //following will ensure that there is an amount to which particle should spawn
        var main = m_particleSystem.main;

        main.maxParticles = m_max;

        var em = m_particleSystem.emission;

        // this will ensure all the particles get spawn at the same time
        em.rateOverTime = m_max;
    }

    //following method will be called once in the level which will manage all the particles according to the position provided by the user
    internal IEnumerator ParticlesPositionController(float waitTime)
    {
        ////managing number of particles according to the number of positions
        ReuseParticles();

        yield return new WaitForSeconds(waitTime);

        m_particleList = new ParticleSystem.Particle[m_particleSystem.particleCount];

        //getting all the particles in to the list of particles
        m_particleSystem.GetParticles(m_particleList);

        //following loop will place each particle to the position provide by the user
        for (int i = 0; i < m_positions.Length; i++)
        {
            try
            {
                //moving the particle to a certain position
                //m_particleList[i].position = Vector3.Lerp(m_particleList[i].position, m_positions[i], Random.Range(particleSpeed/4, particleSpeed) * Time.smoothDeltaTime);
               
                if((i + 2) < m_positions.Length)
                {
                    var p1 = m_positions[i];
                    var p2 = m_positions[i + Random.Range(1, 2)];
                    var v = new Vector3(
                        Random.Range(p1.x, p2.x),
                        p1.y,
                        Random.Range(p1.z, p2.z));
                    m_particleList[i].position = Vector3.Lerp(m_particleList[i].position, v, Random.Range(particleSpeed / 4, particleSpeed) * Time.smoothDeltaTime);

                }
                else
                {
                    m_particleList[i].position = Vector3.Lerp(m_particleList[i].position, m_positions[i], Random.Range(particleSpeed / 4, particleSpeed) * Time.smoothDeltaTime);
                }


                transform.Translate(m_particleList[i].position);
            }
            catch (System.Exception e)
            {
                Debug.Log("Break"+e.ToString());
            }
        }

        //setting each particle in the particle system according to the particles stored in the list
        m_particleSystem.SetParticles(m_particleList, m_particleSystem.particleCount);

        StartCoroutine(ParticlesPositionController(waitTime));
    }

    internal void Particlselife(float lifetime)
    {
        if (m_particleList == null)
            return;

        m_particleList = new ParticleSystem.Particle[m_particleSystem.particleCount];

        //getting all the particles in to the list of particles
        m_particleSystem.GetParticles(m_particleList);

        //following loop will place each particle to the position provide by the user
        for (int i = 0; i < m_positions.Length; i++)
        {
            try
            {
                m_particleList[i].startLifetime = lifetime;
                m_particleList[i].remainingLifetime = lifetime;
            }
            catch (System.Exception e)
            {
                Debug.Log(e.ToString());
            }
        }

        //setting each particle in the particle system according to the particles stored in the list
        m_particleSystem.SetParticles(m_particleList, m_particleSystem.particleCount);

        //transform.Rotate(ProtagonistScript.m_instance.m_controller.VirtualPosition());

    }

}
