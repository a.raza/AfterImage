﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;

public class VerticesHolder : MonoBehaviour 
{
    Mesh _mesh;
    Vector3[] _verts;
    //float a_time;

    public ParticleManagerScript pm;

    [Range(0.35f, 10)]
    public float spaceBetweenParticles;

    private List<Vector3> m_positions = new List<Vector3>();
    private MeshFilter mf;
    void Start()
    {   
        mf = GetComponent<MeshFilter>();

        MakeNewMesh(mf);
    }

    //void FixedUpdate()
    //{
    //    MakeNewMesh(mf);
    //}

    //making new mesh that is writable..
    MeshFilter MakeNewMesh(MeshFilter a_mf)
    {
        //getting the mesh data and adding into a new mesh
        _mesh = a_mf.sharedMesh; //sharedmesh is used for reading mesh data

        //creating an array of int,
        //according to the number of triangles in mesh
        var triangles = _mesh.triangles;

        //creating an array of vecter 3,
        //according to the total number of vertix points in the mesh  
        var vertices = new Vector3[triangles.Length];

        //copying all the vertex points in the mesh, into temp var
        var oldVerts = _mesh.vertices;

        //Adding all the vertex points into Vector3 array,
        //and triangles in the mesh, into int array, one by one
        for (var i = 0; i < triangles.Length; i++)
        {
            vertices[i] = oldVerts[triangles[i]];

            triangles[i] = i;
        }

        //assigning the vertices of the new mesh with the vector3 array
        _mesh.vertices = vertices;

        //assigning the triangles of the new mesh with the int array
        _mesh.triangles = triangles;

        //After modifying the vertices..
        //it is often useful to update the normals to reflect the change
        _mesh.RecalculateNormals();

        //getting the vertices to update them in realtime..
        _verts = _mesh.vertices;

        Debug.Log(_verts.Length);

        UpdateParticlesWithVertices();

        //transform.position *= 5;
        return a_mf;
    }


    private void UpdateParticlesWithVertices()
    {
        for (int i = 0; i < _verts.Length; i++)
        {
            if(!m_positions.Contains(_verts[i] + transform.position))
                m_positions.Add(_verts[i] + transform.position);
        }

        //GapEliminator();

        pm.m_positions = m_positions.ToArray();

        pm.gameObject.SetActive(true);

        Debug.Log("Particles Count: " + m_positions.Count);

    }

    public void GapEliminator()
    {
        for (int x = 0; x < m_positions.Count; x++)
        {
            var addon = x + 1;

            if (addon >= m_positions.Count)
                break;

            PlaceParticlesBetwee(m_positions[x], m_positions[addon]);

            addon = x + 1;

            x++;

            if (addon >= m_positions.Count)
                break;

            PlaceParticlesBetwee(m_positions[x], m_positions[addon]);
        }
    }


    private void PlaceParticlesBetwee(Vector3 A, Vector3 B) // fils the gap between the positions
    {
        float distance = (A - B).magnitude;

        for (float space = 0; space < (distance);)
        {
            var a_position = (Lerp(A, B, space / distance));

            m_positions.Add(a_position);

            space += spaceBetweenParticles;
        }
    }

    private Vector3 Lerp(Vector3 start, Vector3 end, float percent)
    {
         return (start + percent*(end - start));
    }
 
}
